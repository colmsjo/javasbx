public class FirstLambdaExpression {
    public String variable = "Class Level Variable";

    public void lambdaExpression(){
        String variable = "Method Local Variable";
        String nonFinalVariable = "This is non final variable";
        new Thread (() -> {
            //Below line gives compilation error
            //String variable = "Run Method Variable"
            System.out.println("->" + variable);
            System.out.println("->" + this.variable);
       }).start();
    }

    public static void main(String[] arg) {
        new FirstLambdaExpression().lambdaExpression();
    }

}
