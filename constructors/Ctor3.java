import java.lang.reflect.Constructor;
import java.lang.NoSuchMethodException;

public class Ctor3 {
  public static void main(String[] args){
    Class<?> c = Foo.class;

    try {
      Constructor<?> ctors[] = c.getConstructors();
      Constructor<?> ctor = c.getConstructor();

      ((Foo) ctors[0].newInstance()).print();
      ((Foo) ctors[1].newInstance("abc")).print();

      ((Foo)ctor.newInstance()).print();
    } catch (Exception e) {
      e.printStackTrace();
    }


  }
}

class Foo {
  String s;

  public Foo(){}

  public Foo(String s){
    this.s=s;
  }

  public void print() {
    System.out.println(s);
  }
}
