import java.lang.reflect.Constructor;
import java.lang.NoSuchMethodException;

class P {
  String s;
  public P(){}
}

public class Ctor {

  public static void main(String[] args){
    try {
      System.out.println(P.class.getConstructor().newInstance());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
