import java.lang.reflect.Field;

public class Iterate {

  public class Person {
    public String name;
    public String contact;
    public String address;

    public Person(){}
  }

  public static void main(String[] args) throws IllegalAccessException {
    (new Iterate()).run();
  }

  public void run() throws IllegalAccessException {
    Person p = new Person();

    for (Field field : p.getClass().getDeclaredFields()) {
      //field.setAccessible(true); // if you want to modify private fields
      
      System.out.println(field.getName()
               + " - " + field.getType()
               + " - " + field.get(p));
    }
  }

}
