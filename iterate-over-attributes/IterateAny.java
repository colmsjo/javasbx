import java.lang.reflect.Field;
import java.util.ArrayList;

public class IterateAny {

  public class Person {
    public String name = "kalle";
    public String contact = "karlsson";
    public String address = "lilla gatan 11";
  }

  public static void main(String[] args) throws IllegalAccessException {
    (new IterateAny()).run();
  }


  public void traverse(Object o) {
    try {
      for (Field f: o.getClass().getDeclaredFields()) {
          if(f.getType() == "".getClass())
            System.out.println("STRING");

         System.out.println(f.getName()
                   + " - " + f.getType()
                   + " - " + f.get(o));
      }
    } catch(Exception e) {
      System.out.println("ERROR!!");
    }
  }

  public void run() throws IllegalAccessException {
    ArrayList<Person> v = new ArrayList<Person>();
    v.add(new Person());
    v.add(new Person());

    // Loose the type
    ArrayList<?> w = v;

    System.out.println("v: " + v);
    System.out.println("w:" + w);

    for(Object o: w) {
      traverse(o);
    }
  }

}
