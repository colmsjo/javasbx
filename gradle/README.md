Build project: `gradle build`

RUn project: `gradle run`

List tasks: `gradle tasks`

Show versions etc: `gradle dependencies`

Project properties: `gradle properties`

Generate project info: `gradle forceTest --profile`

See `build/reports` for more information from the build.
