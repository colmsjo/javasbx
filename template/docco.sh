#!/bin/bash
# bash 4 is needed to use the globstar option: `brew install bash; brew ls bash` 
# see http://mistermorris.com/blog/get-yourself-globstar-bash-4-for-your-mac-terminal/
# docco needs to be installed globally: `npm install -g docco`
shopt -s globstar
docco -o docco ./src/**/*.java
