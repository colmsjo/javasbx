From http://www.java8.org/introduction-to-java-8-lambda-expressions.html

Links:

 * http://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
 * http://www.informit.com/articles/article.aspx?p=2171751&seqNum=3
 * http://java.dzone.com/articles/java-lambda-expressions-basics

```
java> public int add(int a, int b) {return a+b;}
Created method int add(int, int)
java> add(1,2)
java.lang.Integer res0 = 3
```

With lambdas:

```
java> public BinaryOperator<Integer> add2 = (a,b) -> a+b;
java.util.function.BinaryOperator<java.lang.Integer> add2 = Evaluation$hay0nq3gi1fs6ve9dkp4$$Lambda$2/540847780@54c1692d


```



```
public static <T> Collection<T> filter(Predicate<T> predicate,
                                       Collection<T> items) {
    Collection<T> result = new ArrayList<T>();
    for(T item: items) {
        if(predicate.test(item)) {
            result.add(item);
        }
    }
    return result;
}

Collection<Integer> myInts = asList(0,1,2,3,4,5,6,7,8,9);
Collection<Integer> onlyOdds = filter(n -> n % 2 != 0, myInts);
```
