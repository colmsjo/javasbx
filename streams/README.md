Streams in Java
--------------

I typically use javarepl when experimenting.

Combine operations:

```
List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");
myList.stream().forEach(System.out::println);
myList.stream().filter(s -> s.startsWith("c")).map(String::toUpperCase).sorted().forEach(System.out::println);
```

`map`, `filter` and `reduce` on Integers:

```
List<Integer> nums = Arrays.asList(1,2,3,4,5,6,7,8,9);
nums.stream().map( (a) -> a*a ).forEach(System.out::println);
nums.stream().filter(n -> n % 2 != 0).forEach(System.out::println);
nums.stream().reduce((a, b) -> a+b).ifPresent(System.out::println);
```

`reduce` on classes:

```
class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() { return name; }
}

Person max = new Person("Max", 18);

List<Person> persons = Arrays.asList(new Person("Max", 18),new Person("Peter", 23),new Person("Pamela", 23),new Person("David", 12));
Integer ageSum = persons.stream().reduce(0, (sum, p) -> sum += p.age, (sum1, sum2) -> sum1 + sum2);
```

`reduce` explained:

```
persons.stream().reduce(0,
        (sum, p) -> {
            System.out.format("accumulator: sum=%s; person=%s\n", sum, p);
            return sum += p.age;
        },
        (sum1, sum2) -> {
            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
            return sum1 + sum2;
        });
```

`parallelStream` makes it possible execute calculations in parallel.

```
persons.parallelStream().reduce(0,
        (sum, p) -> {
            System.out.format("accumulator: sum=%s; person=%s\n", sum, p);
            return sum += p.age;
        },
        (sum1, sum2) -> {
            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
            return sum1 + sum2;
        });
```

`flatMap` can be used to 'flatten out' nested structures, for instance streams
of streams:

```
import java.util.stream.*;

class Bar {
    String name;
    Bar(String name) { this.name = name; }
}

class Foo {
    String name;
    List<Bar> bars = new ArrayList<>();
    Foo(String name) { this.name = name; }
}

List<Foo> foos = new ArrayList<>();

// [Foo1, Foo2, Foo3]
IntStream.range(1, 4).forEach(i -> foos.add(new Foo("Foo" + i)));

// [[Bar1<-Foo1, ..., Bar3<-Foo1], ..., [Bar1<-Foo3, ...]]
foos.forEach(f -> IntStream.range(1, 4).forEach(i -> f.bars.add(new Bar("Bar" + i + " <- " + f.name))));

// [Bar1<-Foo1, Bar2<-Foo1, Bar3<-Foo1, Bar1<-Foo2, ...]
foos.stream().flatMap(f -> f.bars.stream()).forEach(b -> System.out.println(b.name));
```

Links:

 * http://winterbe.com/posts/2014/07/31/java8-stream-tutorial-examples/
 * http://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html
