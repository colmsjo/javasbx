import java.util.*;
import java.util.stream.*;

class Tmp {

  public static void main(String[] args) {

class Bar {
    String name;
    Bar(String name) { this.name = name; }
}

class Foo {
    String name;
    List<Bar> bars = new ArrayList<>();
    Foo(String name) { this.name = name; }
}

List<Foo> foos = new ArrayList<>();
IntStream.range(1, 4).forEach(i -> foos.add(new Foo("Foo" + i)));
foos.forEach(f -> IntStream.range(1, 4).forEach(i -> f.bars.add(new Bar("Bar" + i + " <- " + f.name))));
foos.stream().flatMap(f -> f.bars.stream()).forEach(b -> System.out.println(b.name));


  }
}
