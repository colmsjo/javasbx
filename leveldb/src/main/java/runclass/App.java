package runclass;

import org.iq80.leveldb.*;
import static org.fusesource.leveldbjni.JniDBFactory.*;
import java.io.*;

//
// Hello world!
//
public class App {
  public static void main( String[] args ) throws IOException {
    System.out.println( "-- Testing LevelDB in Java" );

    Options options = new Options();
    options.createIfMissing(true);
    DB db = factory.open(new File("example-leveldb"), options);
    try {
      // Use the db in here....
      db.put(bytes("Tampa"), bytes("rocks"));
      System.out.println("get after put: " + asString(db.get(bytes("Tampa"))));
      db.delete(bytes("Tampa"));
      System.out.println("get after delete: " + asString(db.get(bytes("Tampa"))));
    } finally {
      // Make sure you close the db to shutdown the
      // database and avoid resource leaks.
      db.close();
    }

  }
}
