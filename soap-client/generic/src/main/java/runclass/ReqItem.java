package runclass;

import java.util.Hashtable;
import java.util.Vector;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.KvmSerializable;

class ReqItem implements KvmSerializable {

  class C {
    public Object value;
    public Class type;
    public String name;
  }

  private String key;
  private Hashtable<String,C> hasht;
  private Vector<C> vect;

  public ReqItem() {
  }

  public ReqItem(String key) {
    this.key = key;
    this.hasht = new Hashtable<String, C>();
    this.vect = new Vector<C>();
  }

  public void set(String name, Object value, Class type) {
    C containter = new C();
    containter.name = name;
    containter.value = value;
    containter.type = type;
    this.hasht.put(name, containter);
    this.vect.add(containter);
  }

  public Object getProperty(int idx) {
    if(idx < 0 || idx >= this.vect.size()) return null;
    return this.vect.get(idx).value;
  }

  public int getPropertyCount() {
    return this.vect.size();
  }

  public void getPropertyInfo(int idx, Hashtable arg1, PropertyInfo info) {
    if(idx < 0 || idx >= this.vect.size()) return;

    info.type = this.vect.get(idx).type;
    info.name = this.vect.get(idx).name;
  }

  // skippng setProperty (isn't used here)
  public void setProperty(int index, Object value) {
  }

  public void addToRequest(SoapObject request) {
    PropertyInfo pi = new PropertyInfo();
    pi.setName(this.key);
    pi.setValue(this);
    pi.setType(this.getClass());
    request.addProperty(pi);
  }

}
