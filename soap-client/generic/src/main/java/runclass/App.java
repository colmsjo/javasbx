/**
 *
 * kSOAP docs: http://kobjects.org/ksoap2/doc/api/overview-summary.html
 * kSOAP2-android: http://simpligility.github.io/ksoap2-android/index.html
 *
 */

package runclass;

import java.util.Hashtable;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class App {
  public static void main( String[] args ){
    (new App()).run();
  }

  public void run() {
    System.out.println( "Hello SOAP World!" );
    bankidService srv = new bankidService(false);
    System.out.println("" + srv.WebServiceCallExample());
  }

  class bankidRes {
    public int status;
    public String transactionId;
    public String orderRef;
    public String AutoStartToken;
    public String faultstring;

    public String toString() {
        return "status: " + this.status +
             ", transactionId: " + this.transactionId +
             ", orderRef " + this.orderRef +
             ", AutoStartToken " + this.AutoStartToken +
             ", faultstring " + this.faultstring;
    }
  }

  class bankidService {
    private boolean debug;

    public bankidService(boolean debug) {
      this.debug = debug;
    }

    public bankidRes WebServiceCallExample(){
      String NAMESPACE = "urn:riv:Hemleverans:bankidAuthenticateResponder:1";
      String METHOD_NAME = "bankidAuthenticateRequest";
      String SOAP_ACTION = NAMESPACE;
      String URL = "http://localhost:8080/ws";

      SoapObject soapRequest = new SoapObject(NAMESPACE, METHOD_NAME);

      ReqItem req = new ReqItem("pnr");
      req.set("pnr", "199001018888", PropertyInfo.STRING_CLASS);
      req.addToRequest(soapRequest);

      bankidRes res = new bankidRes();

      SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
      envelope.dotNet = true;
      envelope.setOutputSoapObject(soapRequest);

      envelope.addMapping(NAMESPACE, "bankidAuthenticateRequest", new ReqItem().getClass());
      HttpTransportSE httpTransport = new HttpTransportSE(URL);

      try {
        httpTransport.debug = this.debug;

        httpTransport.call(SOAP_ACTION, envelope);

        if(this.debug) {
          System.out.println("Dump request: " + httpTransport.requestDump);
          System.out.println("Dump response: " + httpTransport.responseDump);
        }

        Vector<?> responseVector = (Vector<?>) envelope.getResponse();

        res.status =  Integer.parseInt(responseVector.get(0).toString());
        res.transactionId =  responseVector.get(1).toString();
        res.orderRef = responseVector.get(2).toString();
        res.AutoStartToken = responseVector.get(3).toString();
        res.faultstring = responseVector.get(4).toString();

        if(this.debug) {
          System.out.println("Dump result object: " + res.toString() );
        }

      }
      catch(Exception e) {
        System.out.println("Dump request: " + httpTransport.requestDump);
        System.out.println("Dump response: " + httpTransport.responseDump);
        e.printStackTrace();
      }

      return res;
    }
  }
}
