/**
 *
 *
 * kSOAP docs: http://kobjects.org/ksoap2/doc/api/overview-summary.html
 * kSOAP2-android: http://simpligility.github.io/ksoap2-android/index.html
 *
 */

package runclass;

import java.util.Hashtable;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.SoapSerializationEnvelope;


public class App {
  public static void main( String[] args ){
    (new App()).run();
  }

  public void run() {
    System.out.println( "Hello SOAP World!" );
    bankidService srv = new bankidService();
    srv.WebServiceCallExample();
  }
	//
	// http://seesharpgears.blogspot.se/2010/10/ksoap-android-web-service-tutorial-with.html
	//

  class bankidReq implements KvmSerializable {
    public String pnr;

    public bankidReq(){
    }

    public bankidReq(String pnr) {
      this.pnr = pnr;
    }

    public Object getProperty(int arg0) {

      switch(arg0)
      {
      case 0:
        return this.pnr;
      }

      return null;
    }

    public int getPropertyCount() {
      return 1;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
      switch(index)
      {
      case 0:
        info.type = PropertyInfo.INTEGER_CLASS;
        info.name = "pnr";
        break;
        default: break;
      }
    }

    public void setProperty(int index, Object value) {
      switch(index)
      {
      case 0:
        this.pnr = value.toString();
        break;
      default:
        break;
      }
    }

  }

  class bankidRes implements KvmSerializable {
    public int status;
    public String transactionId;
    public String orderRef;
    public String AutoStartToken;
    public String faultstring;

    public bankidRes(){
    }

    public bankidRes(int status, String transactionId, String orderRef,
                      String AutoStartToken, String faultstring) {

      this.status = status;
      this.orderRef = orderRef;
      this.faultstring = faultstring;
      this.transactionId = transactionId;
      this.AutoStartToken = AutoStartToken;
    }

    public Object getProperty(int arg0) {

      switch(arg0)
      {
      case 0:
        return this.status;
      case 1:
        return this.transactionId;
      case 2:
        return this.orderRef;
      case 3:
        return this.AutoStartToken;
      case 4:
        return this.faultstring;
      }

      return null;
    }

    public int getPropertyCount() {
      return 5;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
      switch(index)
      {
      case 0:
        info.type = PropertyInfo.INTEGER_CLASS;
        info.name = "status";
        break;
      case 1:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "transactionId";
        break;
      case 2:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "orderRef";
        break;
      case 3:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "AutoStartToken";
        break;
      case 4:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "faultstring";
        break;
      default: break;
      }
    }

    public void setProperty(int index, Object value) {
      switch(index)
      {
      case 0:
        this.status = Integer.parseInt(value.toString());
        break;
      case 1:
        this.transactionId = value.toString();
        break;
      case 2:
        this.orderRef = value.toString();
        break;
      case 3:
        this.AutoStartToken = value.toString();
        break;
      case 4:
        this.faultstring = value.toString();
        break;
      default:
        break;
      }
    }

    public String toString() {
        return "status: " + this.status +
             ", transactionId: " + this.transactionId +
             ", orderRef " + this.orderRef +
             ", AutoStartToken " + this.AutoStartToken +
             ", faultstring " + this.faultstring;
    }

  }

  class bankidService {

    public void WebServiceCallExample(){
      String NAMESPACE = "urn:riv:Hemleverans:bankidAuthenticateResponder:1";
      String METHOD_NAME = "bankidAuthenticateRequest";
      String SOAP_ACTION = NAMESPACE;
      String URL = "http://localhost:8080/ws";

      SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

      bankidReq req = new bankidReq();
      req.pnr = "199001018888";

      bankidRes res = new bankidRes();

      PropertyInfo pi = new PropertyInfo();
      pi.setName("pnr");
      pi.setValue(req);
      pi.setType(req.getClass());
      Request.addProperty(pi);

      /*
       * Set the web service envelope
       *
       * */
      SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
      envelope.dotNet = true;
      envelope.setOutputSoapObject(Request);

      envelope.addMapping(NAMESPACE, "bankidAuthenticateRequest", new bankidReq().getClass());
      HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

      /*
       * Call the web service and retrieve result ... how luvly <3
       *
       * */
      try
      {
        androidHttpTransport.debug = true;

        androidHttpTransport.call(SOAP_ACTION, envelope);

        System.out.println("dump Request: " + androidHttpTransport.requestDump);
        System.out.println("dump response: " + androidHttpTransport.responseDump);


//        SoapObject response = (SoapObject) envelope.getResponse();
        Vector<?> responseVector = (Vector<?>) envelope.getResponse();
        System.out.println(responseVector);
        res.status =  Integer.parseInt(responseVector.get(0).toString());
        res.transactionId =  responseVector.get(1).toString();
        res.orderRef = responseVector.get(2).toString();
        res.AutoStartToken = responseVector.get(3).toString();
        res.faultstring = responseVector.get(4).toString();

/*        res.status =  Integer.parseInt(response.getProperty(0).toString());
        res.transactionId =  response.getProperty(1).toString();
        res.transactionId = response.getProperty(2).toString();
        res.AutoStartToken = response.getProperty(3).toString();
        res.faultstring = response.getProperty(4).toString();
*/
/*
        SoapObject response =  (SoapObject) envelope.bodyIn;
        Vector<?> responseVector = (Vector<?>) response.getProperty(0);

        int count = responseVector.size();
        System.out.println("SIZE OF RESULT: " + count);

        for (int i = 0; i < count; ++i) {
          SoapObject test = (SoapObject) responseVector.get(i);
          res.status  = Integer.parseInt(response.getProperty(0).toString());
        }
*/
        System.out.println( res.toString() );
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
  }
}
