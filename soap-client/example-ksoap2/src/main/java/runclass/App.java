/**
 *
 *
 * kSOAP docs: http://kobjects.org/ksoap2/doc/api/overview-summary.html
 * kSOAP2-android: http://simpligility.github.io/ksoap2-android/index.html
 *
 */

package runclass;

import java.util.Hashtable;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class App {
  public static void main( String[] args ){
    (new App()).run();
  }

  public void run() {
    System.out.println( "Hello SOAP World!" );
    Category c = new Category();
    c.WebServiceCallExample();
  }
	//
	// http://seesharpgears.blogspot.se/2010/10/ksoap-android-web-service-tutorial-with.html
	//

  class Category implements KvmSerializable
  {
    public int CategoryId;
    public String Name;
    public String Description;

    public Category(){
    }

    public Category(int categoryId, String name, String description) {

      CategoryId = categoryId;
      Name = name;
      Description = description;
    }


    public Object getProperty(int arg0) {

      switch(arg0)
      {
      case 0:
        return CategoryId;
      case 1:
        return Name;
      case 2:
        return Description;
      }

      return null;
    }

    public int getPropertyCount() {
      return 3;
    }

    public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
      switch(index)
      {
      case 0:
        info.type = PropertyInfo.INTEGER_CLASS;
        info.name = "CategoryId";
        break;
      case 1:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "Name";
        break;
      case 2:
        info.type = PropertyInfo.STRING_CLASS;
        info.name = "Description";
        break;
      default: break;
      }
    }

    public void setProperty(int index, Object value) {
      switch(index)
      {
      case 0:
        CategoryId = Integer.parseInt(value.toString());
        break;
      case 1:
        Name = value.toString();
        break;
      case 2:
        Description = value.toString();
        break;
      default:
        break;
      }
    }

    public void WebServiceCallExample(){
      String NAMESPACE = "http://vladozver.org/";
      String METHOD_NAME = "GetCategoryById";
      String SOAP_ACTION = "http://vladozver.org/GetCategoryById";
      String URL = "http://192.168.1.3/VipEvents/Services/CategoryServices.asmx";

      SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

      /*
       * Create Category with Id to be passed as an argument
       *
       * */
      Category C = new Category();
      C.CategoryId = 1;

      /*
       * Set the category to be the argument of the web service method
       *
       * */
      PropertyInfo pi = new PropertyInfo();
      pi.setName("C");
      pi.setValue(C);
      pi.setType(C.getClass());
      Request.addProperty(pi);

      /*
       * Set the web service envelope
       *
       * */
      SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
      envelope.dotNet = true;
      envelope.setOutputSoapObject(Request);

      envelope.addMapping(NAMESPACE, "Category",new Category().getClass());
      HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

      /*
       * Call the web service and retrieve result ... how luvly <3
       *
       * */
      try
      {
        androidHttpTransport.call(SOAP_ACTION, envelope);
        SoapObject response = (SoapObject)envelope.getResponse();
        C.CategoryId =  Integer.parseInt(response.getProperty(0).toString());
        C.Name =  response.getProperty(1).toString();
        C.Description = (String) response.getProperty(2).toString();
        System.out.println("CategoryId: " +C.CategoryId + " Name: " + C.Name +
													" Description " + C.Description);
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
  }
}
