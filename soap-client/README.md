SOAP on Android
===============

Alternatives:

 * kSOAP2 - http://simpligility.github.io/ksoap2-android/index.html
 * Axis2 - http://axis.apache.org/axis2/java/core/tools/eclipse/wsdl2java-plugin.html
 * wsimport - http://docs.oracle.com/javase/6/docs/technotes/tools/share/wsimport.html


Resources:

* http://stackoverflow.com/questions/9124995/generating-java-from-wsdl-for-use-on-android-with-ksoap2-android-soap-client
* http://stackoverflow.com/questions/297586/how-to-call-soap-web-service-in-android
