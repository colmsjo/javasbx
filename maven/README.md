Generate new project:
`mvn archetype:generate -DgroupId=com.gizur.app1 -DartifactId=app1 -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`

Compile the project: `mvn compile`

Run the tests: `mvn test`

Generate site with info abou the project: `mvn site`

Run main: `mvn exec:java -Dexec.mainClass="com.gizur.app1.App" -Dexec.args="arg0 arg1 arg2"`
