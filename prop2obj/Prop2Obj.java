/**
 *
 * Compile with: `javac -Xlint:unchecked Prop2Obj.java`
 *
 */

import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class Prop {
  public String name;
  public String value;
  public Class type;

  public Prop(){}
  public Prop(String name, String value, Class type){
    this.name = name;
    this.value = value;
    this.type = type;
  }

  public String toString() {
    return "prop name:" + name + ", value:" + value + ", type:" + type;
  }
}

class Person {
  public Person(){}

  public String firstname;
  public String lastname;
  public Boolean child;
  public Integer age;

  public String toString() {
    return "firstname:" + firstname + ", lastname: " + lastname +
           ", child:" + child + ", age:" + age;
  }
}

public class Prop2Obj {

  public Object toObj(Prop[] ps, Class c) throws InvocationTargetException, NoSuchMethodException,
      InstantiationException, NoSuchFieldException, IllegalAccessException {

    Constructor<?> ctor = c.getConstructor();
    Object o = ctor.newInstance();

    for(Prop p: ps) {
      Field f = c.getDeclaredField(p.name);

      if(p.type == String.class) {
        f.set(o, p.value);
      } else if(p.type == Integer.class) {
        f.set(o, Integer.valueOf(p.value));
      } else if(p.type == Boolean.class) {
        f.set(o, Boolean.valueOf(p.value));
      } else {
        throw new Error("Type not supported: " + p.type);
      }
    }

    return o;
  }

  public void run() {
    try {
      Prop[] ps = {new Prop("firstname", "Kalle", String.class),
                   new Prop("lastname", "Karlsson", String.class),
                   new Prop("child", "false", Boolean.class),
                   new Prop("age", "23", Integer.class)};

      Person o = (Person) toObj(ps, Person.class);
      System.out.println(""+o);

    } catch(Exception e) {
      e.printStackTrace(System.out);
    }
  }

  public static void main(String[] args) {
    (new Prop2Obj()).run();
  }
}
