#!/bin/bash
cp ~/Dropbox/working-docs/FY16/ProvideIT/Frisq/fenix/*.xsd src/main/resources/
cp src/main/countries.xsd src/main/resources/

# for debugging invalid schemas
#rm -f src/main/resources/common.xsd
