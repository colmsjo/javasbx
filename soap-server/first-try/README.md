Simple SOAP server
=================

From: https://spring.io/guides/gs/producing-web-service/

1. Build: `./gradlew build`
2. Run: `java -jar build/libs/frisq-test-server-0.1.0.jar`
3. Test `curl -sS --header "content-type: text/xml" -d @test-client/request.xml http://localhost:8080/ws`
4. Use `tidy` to get readable output. `curl -sS --header "content-type: text/xml" -d @test-client/request.xml http://localhost:8080/ws |tidy -xml -i`
5. Show the WSDL: `curl http://localhost:8080/ws/apptexterresponder.wsdl`

`xmllint` is good for validating that XML files are valid.

NOTE: Detection of changes does not work on XML files. Do `gradlew clean`
followed by `gradlew build`


Resources
---------

* http://docs.spring.io/spring-ws/site/reference/html/server.html
