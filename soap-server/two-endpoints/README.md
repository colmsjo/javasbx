Simple SOAP server
=================

The purpose of this SOAP server is to present SOAP services with the signature
that the Frisq XML files specify. There is currently no logic in this server.
The server is useful for developing apps without being dependent on the Frisq
test server.

Build and test
--------------

1. Build: `./gradlew build`
2. Run: `java -jar build/libs/frisq-test-server-0.1.0.jar`
3. Test `curl -sS --header "content-type: text/xml" -d @test-client/apptexter_request.xml http://localhost:8080/ws`
4. Use `tidy` to get readable output. `curl -sS --header "content-type: text/xml" -d @test-client/apptexter_request.xml http://localhost:8080/ws |tidy -xml -i`
5. Show the WSDL: `curl http://localhost:8080/ws/apptexterresponder.wsdl`

`xmllint` is good for validating that XML files are valid.

NOTE: Detection of changes does not work on XML files. Do `gradlew clean`
followed by `gradlew build`


Notes on the API
----------------

The xml files in the folder `test-client` contains examples of requests. There
is currently only dummy data in these files. The test server will also answer
back with static dummy data.

1. Bankid: `curl -sS --header "content-type: text/xml" -d @test-client/bankid_authenticate_request.xml http://localhost:8080/ws |tidy -xml -i`
