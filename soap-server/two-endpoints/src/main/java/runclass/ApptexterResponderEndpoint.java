package runclass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import riv.hemleverans.ApptexterRequest;
import riv.hemleverans.ApptexterResponse;

@Endpoint
public class ApptexterResponderEndpoint {
	private static final String NAMESPACE_URI = "urn:riv:Hemleverans";
  private int i;

	//@Autowired
	public ApptexterResponderEndpoint() {
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "ApptexterRequest")
	@ResponsePayload
	public ApptexterResponse apptexterRequest(@RequestPayload ApptexterRequest request) {
		ApptexterResponse response = new ApptexterResponse();
    response.setStatus(1);
    response.setCurrentTextVersion("1");

		return response;
	}
}
