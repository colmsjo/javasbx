package runclass;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import riv.hemleverans.BankidAuthenticateRequest;
import riv.hemleverans.BankidAuthenticateResponse;

@Endpoint
public class bankidAuthenticateEndpoint {
	private static final String NAMESPACE_URI = "urn:riv:Hemleverans";
  private int i;

	//@Autowired
	public bankidAuthenticateEndpoint() {
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "bankidAuthenticateRequest")
	@ResponsePayload
	public BankidAuthenticateResponse bankidRequest(@RequestPayload BankidAuthenticateRequest request) {
		BankidAuthenticateResponse response = new BankidAuthenticateResponse();
    response.setStatus(1);
    response.setTransactionId("1");
		response.setOrderRef("1");
		response.setAutoStartToken("1");
		response.setFaultstring("1");

		return response;
	}
}
