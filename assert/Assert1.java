import java.util.*;

public class Assert1 {

  // Must declare throws, nothing is shown otherwise
  public static void main(String[] args) throws Exception {

    assert 1 == 1 : "This should not throw an error";
    assert 1 != 1 : "This should throw an error";

  }
}
